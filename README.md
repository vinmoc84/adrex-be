## Esercizio

### Mini Filesystem Manager

Scopo del test è valutare la conoscenza del framework Spring/Camel risolvendo
l'esercizio descritto d seguito in un tempo limitato.

### Esercizio

Tramite una web app l'utente può interrogare il filesystem per individuare alcune proprietà
dei file.
La web app dovrà essere realizzata in Angular 1+, e fornirà all'utente una semplice select box da cui
potrà selezionare un file e richiedere al be le info su tale file e visualizzarle all'utente.

A tale scopo dovranno essere completate le seguente API

- GET /api/files/ restituisce i files della directory gestita dal be.
- GET /api/files/{nomeFile}/info restituisce le info sul file (es. numero di righe, size, ecc.)
- POST /api/files/{nomeFile} crea un nuovo file.
- DELETE /api/files/{nomeFile} cancella un file

### Modalità di svolgimento
Il candidato può utilizzare tutti gli strumenti a disposizione sia online che offline. 


