package it.beyondtech.interviews.adr.service;

import org.apache.camel.EndpointInject;
import org.apache.camel.FluentProducerTemplate;
import org.springframework.stereotype.Service;

/**
 * $Id$
 *
 * @Author Vincenzo Moccia [vincenzo.moccia@beyondtech.it]
 * Created on 3/20/18.
 */
@Service
public class ReaderService {

    @EndpointInject(uri = "direct:read-file")
    private FluentProducerTemplate producer;


    public int numberOfLines(String fileName) {
        int nLines = 0;

        //TODO IMPLEMENT THE LOGIC

        return nLines;
    }
}
