package it.beyondtech.interviews.adr.service.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * $Id$
 *
 * @Author Vincenzo Moccia [vincenzo.moccia@beyondtech.it]
 * Created on 3/20/18.
 */
@Component
public class CamelRouterReadFile extends RouteBuilder {
    @Override
    public void configure() throws Exception {

    }
}
