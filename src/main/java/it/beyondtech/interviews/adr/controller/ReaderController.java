package it.beyondtech.interviews.adr.controller;

import it.beyondtech.interviews.adr.service.ReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * $Id$
 *
 * @Author Vincenzo Moccia [vincenzo.moccia@beyondtech.it]
 * Created on 3/20/18.
 */
@Controller
@RequestMapping("/api/files")
public class ReaderController {

    @Autowired
    ReaderService readerService;


    /**
     * Restituisce un array con i file contenuti nella directory
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Map> list() {
        HashMap ret = new HashMap();
        //TODO

        //in caso il file non esiste restituire una response con un codice
        //di errore opportuno


        return new ResponseEntity<>(ret, HttpStatus.OK);
    }


    /**
     * Restituisce il numero di righe da un file di testo.
     *
     * @param fileName Nome del file
     * @return un JSON del tipo: {"fileName": "nomefile.txt", "nLines": 100} se la creazione è andata a buon fine
     */
    @RequestMapping(value = "/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Map> info(@PathVariable String fileName) {
        HashMap ret = new HashMap();
        int numberOfLines = readerService.numberOfLines(fileName);
        ret.put("fileName", fileName);
        ret.put("nLines", numberOfLines);

        //in caso il file non esiste restituire una response con un codice
        //di errore opportuno


        return new ResponseEntity<>(ret, HttpStatus.OK);
    }


    /**
     * Crea un nuovo file
     *
     * @param fileName file da creare
     * @return Un JSON del tipo {"fileName": "nomeFile", created: true}
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Map> touchFile(@PathVariable String fileName) {
        HashMap ret = new HashMap();

        return new ResponseEntity<>(ret, HttpStatus.OK);
    }



    /**
     * Cancella un file dalla directory
     * @return
     */
    @RequestMapping(value = "/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Map> delete() {
        HashMap ret = new HashMap();
        //TODO

        //in caso il file non esiste restituire una response con un codice
        //di errore opportuno


        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

}
