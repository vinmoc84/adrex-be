package it.beyondtech.interviews.adr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * $Id$
 *
 * @Author Vincenzo Moccia [vincenzo.moccia@beyondtech.it]
 * Created on 3/20/18.
 */
@SpringBootApplication
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
